This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

## STEPS

## Add ESLINT and Prettier

1. `Add prettier`
   yarn add eslint-config-prettier eslint-plugin-prettier -D
2. `Create a .eslintrc file`
   { "extends": ["react-app", "plugin/prettier/recommended"] }
3. `Add pretty-quick and husky`
   yarn add pretty-quick husky --dev
4. `add this the package.json`
   "husky": {
   "hooks": {
   "pre-commit": "pretty-quick --staged"
   }
   }

(`Watch this video if you have any questions` https://www.youtube.com/watch?v=bfyI9yl3qfE&t=411s)

### Action / Redux -> Dux methods with TypeScript

1. Actions and Reducers: Add your actions and reducer in a file inside the folder called `redux` (for more info take a look at the `redux/session.ts` file)
2. Types: Add your types inside the folder called `Types`. Make sure you name the file with the same name as in the Action/Reducers but with the first letter cpaitalize. So if you call your Actions/Reducer file `session.ts`, then call your Type file `Session.ts`
3. Have fun!

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `yarn test`

### `yarn build`

### `yarn eject`

## Learn More
