import { PostTypes } from "./../redux/posts";
// App Specifiy Types
export interface Post {
  id: number;
  title: string;
  body: string;
}

export interface FetchPostsAction {
  type: PostTypes.FETCH_POSTS;
  payload: Post[];
}

export type PostActionTypes = FetchPostsAction;
