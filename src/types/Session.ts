import { SessionTypes } from "../redux/session";

export interface RegisterUserAction {
  type: SessionTypes.REGISTER_USER;
  data: {
    name: string;
    email: string;
    password: string;
  };
}
export interface RegisterUserSuccessAction {
  type: SessionTypes.REGISTER_USER_SUCCESS;
  user: any; // TODO: "any" is for the moment. The user is a huge object from Firestore. Decide later what we need and specify
  // profile: profile at some point as well
}

export interface RegisterUserFailureAction {
  type: SessionTypes.REGISTER_USER_FAILURE;
  error: string;
}

export type SessionActionTypes =
  | RegisterUserAction
  | RegisterUserFailureAction
  | RegisterUserSuccessAction;
