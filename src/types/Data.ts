export interface Users {
  profile: Profile;
  payments: Payment[];
}

export interface Profile {
  email: string;
  name: string;
  displayName: string;
  phoneNumber: string;
  jurisdiction: string;
  created: number;
  imageUrl: string;
  lastSignInTime: number;
  type: string;
  uid: string;
  isSanctioned: boolean;
}

export interface Payment {
  id: string;
  recipient: Recipient;
  priority: string;
  timestamp: string;
  seqno: number;
  delivered: boolean;
  paymentMessage: PaymentMessage;
}

interface PaymentMessage {
  amount: string;
  currency: string;
  state: string;
  comment: string;
}

interface Recipient {
  id: string;
  name: string;
  displayName: string;
  address: Address;
}

interface Address {
  city: string;
  state: string;
  country: string;
  postcode: string;
  street: string;
  houseNumber: string;
}
