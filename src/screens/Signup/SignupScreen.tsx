import React, { Component } from "react";
import { Link } from "react-router-dom";
import { firestore } from "../../utils/firebase";
import { connect } from "react-redux";
import { registerUser } from "../../redux/session";
import { StoreState } from "../../redux";

interface AppState {
  name: string;
  email: string;
  password: string;
}

interface AppProps {
  registerUser: Function;
  session: any;
}

class SignupScreen extends Component<AppProps, AppState> {
  constructor(props: AppProps) {
    super(props);
    this.state = {
      name: "",
      email: "",
      password: ""
    };
  }

  getDoc = () => {
    const paymentsRef = firestore.collection("payments");
    var docRef = paymentsRef.doc("owedj4dkmXqjlbKEgQJT");
    docRef
      .get()
      .then(function(doc: any) {
        if (doc.exists) {
          console.log("Document data:", doc.data());
        } else {
          // doc.data() will be undefined in this case
          console.log("No such document!");
        }
      })
      .catch(function(error: any) {
        console.log("Error getting document:", error);
      });
  };

  getCollection = () => {
    const paymentsRef = firestore.collection("payments");

    paymentsRef.get().then(function(querySnapshot: any) {
      const payments: any[] = [];
      querySnapshot.forEach(function(doc: any) {
        querySnapshot.forEach((doc: any) => {
          payments.push({
            id: doc.id,
            source: doc.data()
          });
        });
      });
      console.log("Payments ", payments);
    });
  };

  onSubmit = () => {
    const { email, password } = this.state;
    const data = {
      email,
      password
    };
    this.props.registerUser(data);
  };

  componentDidMount() {
    this.getDoc();
    this.getCollection();
  }

  renderInfo = () => {
    //Only for testing purposes
    let info = "";
    const { session } = this.props;
    if (session.isUserAuthenticated === null) info = "Welcome";
    else if (session.loading) info = "Loading...";
    else if (session.error) info = `Error message ${this.props.session.error}`;
    else info = JSON.stringify(this.props.session.user);

    return <div style={{ padding: 20 }}>{info}</div>;
  };
  render() {
    return (
      <div>
        <h2>Estamos live en el internet!</h2>
        <h3>Este es el comienzo de una era chamas</h3>
        <input
          value={this.state.name}
          placeholder="Name"
          onChange={e => this.setState({ name: e.target.value })}
        />
        <input
          value={this.state.email}
          placeholder="Enter email"
          onChange={e => this.setState({ email: e.target.value })}
        />
        <input
          value={this.state.password}
          placeholder="Password"
          onChange={e => this.setState({ password: e.target.value })}
        />
        <button onClick={this.onSubmit}>Submit</button>
        {this.renderInfo()}
        <ul>
          <li>
            <Link to="users">Go to users</Link>
          </li>
          <li>
            <Link to="payments">Go to payments</Link>
          </li>
          <li>
            <Link to="posts">Go to posts</Link>
          </li>
        </ul>
      </div>
    );
  }
}

const mapStateToProps = (state: StoreState, ownProps: AppProps) => {
  return {
    session: state.session
  };
};

export default connect(mapStateToProps, {
  registerUser
})(SignupScreen);
