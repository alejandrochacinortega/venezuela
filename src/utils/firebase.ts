import { firebaseConfig } from "./../config/firebaseConfig";
import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "firebase/analytics";

firebase.initializeApp(firebaseConfig); // Initialize Firebase
firebase.analytics(); // Initialize Firebasefirebase.firebaseInit();

export const firestore = firebase.firestore();
export const auth = firebase.auth();

export default firebase;
