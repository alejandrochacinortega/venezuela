import ReactDOM from "react-dom";
import App from "./components/App";
import { BrowserRouter, Route } from "react-router-dom";

import React from "react";
import { Provider } from "react-redux";
import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import { reducers } from "./redux";
import SignupScreen from "./screens/Signup/SignupScreen";
import PaymentsScreen from "./screens/Payments/PaymentsScreen";
import UsersScreen from "./screens/Users/UsersScreen";

const composeEnhancers = compose(
  applyMiddleware(thunk),
  (window as any).__REDUX_DEVTOOLS_EXTENSION__ &&
    (window as any).__REDUX_DEVTOOLS_EXTENSION__()
);

const store = createStore(reducers, composeEnhancers);

console.log("----------------------------------------");
console.log("ABRIIII ESTAMOS EN LA INTERNET!!!!!");
console.log("ESTAMOS LIVE!!!!!!!!");
console.log("----------------------------------------");

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <Route path="/" exact component={SignupScreen}></Route>
      <Route path="/posts" exact component={App}></Route>
      <Route path="/payments" exact component={PaymentsScreen}></Route>
      <Route path="/users" exact component={UsersScreen}></Route>
    </BrowserRouter>
  </Provider>,
  document.querySelector("#root")
);
