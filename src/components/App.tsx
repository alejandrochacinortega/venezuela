import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchPosts } from "../redux/posts";
import { StoreState } from "../redux";
import { Post } from "../types";

interface AppProps {
  posts: Post[];
  fetchPosts: Function;
}

class App extends Component<AppProps> {
  renderPosts = (): JSX.Element[] => {
    return this.props.posts.map((post: Post) => {
      return (
        <div key={post.id}>
          <p>{post.title}</p>
        </div>
      );
    });
  };

  render() {
    return (
      <div>
        <button onClick={() => this.props.fetchPosts()}>Fetch</button>
        {this.renderPosts()}
      </div>
    );
  }
}

const mapStateToProps = (state: StoreState) => {
  return {
    posts: state.posts
  };
};

export default connect(mapStateToProps, { fetchPosts })(App);
