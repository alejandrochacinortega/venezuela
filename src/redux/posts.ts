import { Post, PostActionTypes } from "../types";
import { Dispatch } from "redux";
import axios from "axios";

const url = "https://jsonplaceholder.typicode.com/posts";

// -- Actions --
export enum PostTypes {
  FETCH_POSTS = "posts/FETCH_POSTS"
}

// -- Actions Creators --
export const fetchPosts = () => {
  return async (dispatch: Dispatch) => {
    const response = await axios.get<Post[]>(url);

    dispatch<PostActionTypes>({
      type: PostTypes.FETCH_POSTS,
      payload: response.data
    });
  };
};

// -- Reducers --
export const postsReducer = (state: Post[] = [], action: PostActionTypes) => {
  switch (action.type) {
    case PostTypes.FETCH_POSTS:
      return action.payload;
    default:
      return state;
  }
};
