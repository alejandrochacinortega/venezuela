import { SessionActionTypes } from "./../types";
import { auth } from "../utils/firebase";
import { Dispatch } from "redux";

// -- Actions --
export enum SessionTypes {
  REGISTER_USER = "session/REGISTER_USER",
  REGISTER_USER_FAILURE = "session/REGISTER_USER_FAILURE",
  REGISTER_USER_SUCCESS = "session/REGISTER_USER_SUCCESS"
}

// -- Action Creators --
export const registerUser = (data: {
  name: string;
  email: string;
  password: string;
}) => {
  return async (dispatch: Dispatch) => {
    try {
      dispatch({ type: SessionTypes.REGISTER_USER, data });
      const response: any = await auth.createUserWithEmailAndPassword(
        data.email,
        data.password
      );
      const { user } = response;
      dispatch({ type: SessionTypes.REGISTER_USER_SUCCESS, user });
    } catch (error) {
      dispatch({
        type: SessionTypes.REGISTER_USER_FAILURE,
        error: error.message
      });
    }
  };
};

// Default state
const defaultState = {
  user: null,
  loading: false,
  error: null,
  isUserAuthenticated: null,
  profile: null
};

// -- Reducer --
export const sessionReducer = (
  state: {} = defaultState,
  action: SessionActionTypes
) => {
  switch (action.type) {
    case SessionTypes.REGISTER_USER:
      return {
        ...state,
        user: null,
        loading: true,
        error: null,
        isUserAuthenticated: false
      };
    case SessionTypes.REGISTER_USER_SUCCESS:
      return {
        ...state,
        user: action.user,
        loading: false,
        error: null,
        isUserAuthenticated: true
      };
    case SessionTypes.REGISTER_USER_FAILURE:
      return {
        ...state,
        user: null,
        loading: false,
        error: action.error,
        isUserAuthenticated: false
      };
    default:
      return state;
  }
};
