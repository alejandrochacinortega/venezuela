import { Post } from "../types";
import { combineReducers } from "redux";
import { postsReducer } from "./posts";
import { sessionReducer } from "./session";

export interface StoreState {
  posts: Post[];
  session: any;
}

export const reducers = combineReducers<StoreState>({
  posts: postsReducer,
  session: sessionReducer
});
